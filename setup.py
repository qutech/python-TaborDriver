from setuptools import setup
import os
import re


def get_version():
    dir_path = os.path.dirname(os.path.realpath(__file__))

    teawg_init = os.path.join(dir_path, 'teawg', '__init__.py')

    with open(teawg_init, 'r') as version_file:
        #  https://packaging.python.org/guides/single-sourcing-package-version/
        version_match = re.search(r"^__version__\s+=\s+['\"]([^'\"]*)['\"]", version_file.read(), re.M)

    if version_match:
        return version_match.group(1)
    else:
        raise RuntimeError("Unable to find version string.")


setup(
    name='pytabor',
    version=get_version(),
    packages=['pytabor', 'teawg'],
    package_dir={'pytabor': 'pytabor', 'teawg': 'teawg'},
    url='http://www.taborelec.com',
    license='GPL',
    author='2016 Tabor-Electronics Ltd.',
    author_email='http://www.taborelec.com/',
    description='Packaged version of the python driver for Tabor Electronics AWGs. Packaged by Simon Humpohl <simon.humpohl@rwth-aachen.de>',
    install_requires=['numpy', 'pyvisa']
)

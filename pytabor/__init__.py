from pyvisa import __version__ as pyvisa_version
from distutils.version import StrictVersion

if StrictVersion(pyvisa_version) >= StrictVersion('1.6'):
    from .pytabor16 import *
    __all__ = pytabor16.__all__
else:
    raise ImportError('Unsupported pyvisa version: {} (should be an "easy" fix)'.format(pyvisa_version))
